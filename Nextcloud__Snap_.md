# Nextcloud (Snap)

```
sudo snap install nextcloud
```

```
sudo nextcloud.manual-install admin password
```

```
sudo nextcloud.occ config:system:set trusted_domains 1 --value=example-ip
```

```
sudo nextcloud.occ config:system:set trusted_domains 1 --value=example.com
```

```
sudo ufw allow 80,443/tcp
```

```
sudo nextcloud.enable-https lets-encrypt
```

## External data directory

```
sudo snap connect nextcloud:removable-media
```

```
sudo nano /var/snap/nextcloud/current/nextcloud/config/config.php
```

\--Edit the data directory entry to point to your external data ex: /mnt/data/nextcloud/data

```
sudo snap start nextcloud
```

## Fixing Data Directory Permissions

```
sudo chown -R root:root /mnt/data/nextcloud 
```

```
sudo chmod -R 0770 /mnt/data/nextcloud 
```

```
sudo snap restart nextcloud
```

## Empty Default Files

Need to edit config.php located here: /var/snap/nextcloud/current/nextcloud/config/config.php

```
'skeletondirectory' => '',
```