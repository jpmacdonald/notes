# Nextcloud-PODMAN

Podman allows you to store --env-files at location

```
# If you do not have firewalld installed
rpm-ostree install firewalld
systemctl reboot

sudo firewall-cmd --add-forward-port=port=80:proto=tcp:toport=8080
sudo firewall-cmd --add-forward-port=port=443:proto=tcp:toport=8443
sudo firewall-cmd --list-all

# If satisfied run this to enable permanently:
sudo firewall-cmd --runtime-to-permanent
sudo systemctl restart firewalld	
sudo systemctl status firewalld
```

## Create storage volumes

```
export PODNAME="nextcloud"
export DATA="/mnt/data"
mkdir -p /mnt/data/containers/nextcloud/{db,nginx,html}
mkdir -p /mnt/data/containers/nextcloud/nginx/{certs,html,modsec-confs,modsec-crs-confs,server-confs}
```

## Create the pod

```
podman pod create --hostname ${PODNAME} --name ${PODNAME} -p 8080:80 -p 8443:8443
```

## Nginx

```
podman unshare chown 101:101 ~/containers/nextcloud/nginx/certs
```

```
curl -o ~/containers/nextcloud/nginx/nginx.conf https://raw.githubusercontent.com/nextcloud/docker/master/.examples/docker-compose/with-nginx-proxy/mariadb-cron-redis/fpm/web/nginx.conf

podman run \
-d --restart=always --pod=${PODNAME} \
-v ${DATA}/containers/nextcloud/html:/var/www/html:ro,z \
-v ${DATA}/containers/nextcloud/nginx/nginx.conf:/etc/nginx/nginx.conf:ro,Z \
--name=${PODNAME}-nginx docker.io/library/nginx:alpine
```

## MariaDB

```
podman run --name mariadb \
--pod nextcloud_pod \
-v ${DATA}/containers/nextcloud/db:/var/lib/mysql:Z \
-e MYSQL_USER="nextcloud" \
-e MYSQL_PASSWORD="sql-password" \
-e MYSQL_ROOT_PASSWORD="sql-root-password" \
-e MYSQL_DATABASE="nextcloud" \
-d --restart=always \
docker.io/library/mariadb:latest
```

## Nextcloud App

```
podman run --name nextcloud \
--pod nextcloud_pod \
-v ~/containers/nextcloud/html:/var/www/html:z \
-d --restart=always \
-e MYSQL_HOST="127.0.0.1" \
-e MYSQL_USER="nextcloud" \
-e MYSQL_PASSWORD="sql-password" \
-e MYSQL_DATABASE="nextcloud" \
docker.io/library/nextcloud:fpm-alpine
```

## Test Nextcloud App

```
podman logs -f nextcloud
```