# Plex Media Server (Rootless Podman) Install

Using Fedora Server 34, Cockpit

```
hostnamectl set-hostname <server-name>
```

## Initial Setup

```
sudo dnf upgrade -y
sudo dnf install cockpit-podman -y
```

Now use your device to connect to server through cockpit at <ip-address>:9090 OR ssh OR directly.

## Example directory:

/mnt/data

* /plex
* /nextcloud
* /movies
* /music
* /tv

## Mount BTRFS RAID 1 Filesystem

```
# Create mount point for our data directory
sudo mkdir -p /mnt/data

# Get UUID of single btrfs volume (you only need to mount one)
lsblk -f

# Copy UUID and place in /etc/fstab
sudo nano /etc/fstab

# At end of line add:
UUID=<UUID HERE>	/mnt/data	btrfs	defaults	0 0 

# Reboot and ls to see if volumes have been mounted
ls /mnt/data
```

## Plex Setup

```
# Create user plex
sudo useradd plex
# The following commands are probably not necessary
sudo groupadd plex 
sudo usermod -a -G plex plex

# Assign id of `plex` user to PLEX_UID
PLEX_UID=$(id -u plex)
# Assign id of `plex` group to PLEX_GID
PLEX_GID=$(id -g plex)
# Change ownership in podman namespace
podman unshare chown $PLEX_UID:$PLEX_GID /mnt/data

# If you now run ls -al on the /media directory you should see an id in the hundred of 
# thousands, e.g. 101000. This is because Podman has set the owner of that directory to
# the user id of media within to the Podman namespace.

# Change to plex user
sudo su plex
# Make directory for storing data/db/config files for plex
mkdir /mnt/data/plex
# As usual, run podman unshare
podman unshare chown -R $PLEX_UID:$PLEX_UID /mnt/data/
```

# First Run of Plex Podman

Change user to `plex` .

Make sure directories are chowned and unshared for SELinux & permissions.

```
sudo su plex
cd
```

Now use podman to run a container.

```
podman run -d \
  --name=plex \
  --detach \
  --network=host \
  -e PUID=PLEX_UID \
  -e PGID=PLEX_GID \
  -e TZ=America/New_York \
  -e PLEX_CLAIM="<CLAIM CODE>" \
  -v /mnt/data:/plex/config:Z \
  -v /mnt/data/movies:/movies:Z \
  -v /mnt/data/tv:/tv:Z \
  -v /mnt/data/music:/music:Z \
  --restart unless-stopped \
  --label "io.containers.autoupdate=registry" \
ghcr.io/linuxserver/plex:latest
```

`:Z` and `:z` are labels for SELinux to know whether to share or not share directory/files with other containers

We use \`\`:Z`to denote that the path will only be used by the plex container. So,`:z\` would mean the path may be shared by other containers.

The `--label "io.containers.autoupdate=registry"` allows us to autoupdate the container using a systemd unit file.

Claim code is receieved from https://plex.tv/claim

### Run the container and use:

`podman logs <container-name>` to see if all is well.

If all is well, access the web ui at https://localhost:32400/web

## Setup systemd startup

By default the user needs to be logged in to run `systemctl --user`. To enable this we use loginctl.

```
# As root or superuser
sudo loginctl enable-linger plex
sudo su plex
# Create folder to hold systemd configurations
mkdir -p ~/.config/systemd/user/
# Generate plex service entry
podman generate systemd --name plex > ~/.config/systemd/user/plex.service
```

In each generated file, replace the `WantedBy` line:

```
[Install]
WantedBy=multi-user.target
```

With the the `default` target:

```
[Install]
WantedBy=default.target
```

```
# Reload systemctl daemon for it to see changes.
systemctl --user daemon-reload
# Enable plex
systemctl --user enable --now plex.service
```

To make sure everything is working reboot.

```
# Ensure your podman container is running
podman ps
# Ensure your systemd service is running
systemctl --user status plex.service
```

If there are issues running systemctl, `export XDG_RUNTIME_DIR=/run/user/"$(id -u)"`

## Firewall

```
sudo nano /etc/firewalld/services/plexmediaserver.xml
```

```
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>plexmediaserver</short>
  <description>Ports required by plexmediaserver.</description>
  <port protocol="tcp" port="32400"></port>
  <port protocol="udp" port="1900"></port>
  <port protocol="tcp" port="3005"></port>
  <port protocol="udp" port="5353"></port>
  <port protocol="tcp" port="8324"></port>
  <port protocol="udp" port="32410"></port>
  <port protocol="udp" port="32412"></port>
  <port protocol="udp" port="32413"></port>
  <port protocol="udp" port="32414"></port>
  <port protocol="tcp" port="32469"></port>
</service>
```

```
sudo firewall-cmd --add-service=plexmediaserver --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
```

SSH HTTP TUNNEL:

```
ssh jimmy@<ip-address> -L 8080:localhost:32400
```

## Updating

Plex container should update itself upon restart, if not simply:

```
podman stop plex
podman pull plexinc/pms-docker:latest
# use the previous run command to start new container
podman run -d ... \
```

Everything else is already set up, so that should be it.

```
--label "io.containers.autoupdate=registry"
# using the above label in our run command we can simply run the following command for future updates

# Not only does the image need to be run with the label tag, it also needs a fully qualified image reference (e.g., quay.io/podman/stable:latest

sudo podman auto-update
```